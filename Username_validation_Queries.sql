DELETE FROM DIGX_FW_CONFIG_ALL_B WHERE PROP_ID = 'FCDB_USERNAME_VALIDATE_MOCK' and CATEGORY_ID = 'OriginationConfig';
Insert into DIGX_FW_CONFIG_ALL_B (PROP_ID,CATEGORY_ID,PROP_VALUE,FACTORY_SHIPPED_FLAG,PROP_COMMENTS,SUMMARY_TEXT,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATED_DATE,OBJECT_STATUS,OBJECT_VERSION_NUMBER,EDITABLE,CATEGORY_DESCRIPTION) values ('FCDB_USERNAME_VALIDATE_MOCK','OriginationConfig','FALSE','Y','FCDB Username Validation Mock','FCDB Username Validation Mock','profinchuser',SYSDATE,'profinchuser',SYSDATE,'A',1,'N','Origination');

------------ Please update the JDBC url with the approriate values : HOSTIP , HOSTPORT and SID ---------------------------
DELETE FROM DIGX_FW_CONFIG_ALL_B WHERE PROP_ID = 'FCDB_JDBC_URL' and CATEGORY_ID = 'OriginationConfig';
Insert into DIGX_FW_CONFIG_ALL_B (PROP_ID,CATEGORY_ID,PROP_VALUE,FACTORY_SHIPPED_FLAG,PROP_COMMENTS,SUMMARY_TEXT,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATED_DATE,OBJECT_STATUS,OBJECT_VERSION_NUMBER,EDITABLE,CATEGORY_DESCRIPTION) values ('FCDB_JDBC_URL','OriginationConfig','jdbc:oracle:thin:@HOSTIP:HOSTPORT:SID','Y',null,'FCDB JDBC URL','profinchuser',SYSDATE,'profinchuser',SYSDATE,'A',1,'N',null);

------ Please add OBDX schema name 'OBDX_SCHEMA' ------------------
DELETE FROM DIGX_FW_CONFIG_ALL_B WHERE PROP_ID = 'FCDB_SCHEMANAME' and CATEGORY_ID = 'OriginationConfig';
Insert into DIGX_FW_CONFIG_ALL_B (PROP_ID,CATEGORY_ID,PROP_VALUE,FACTORY_SHIPPED_FLAG,PROP_COMMENTS,SUMMARY_TEXT,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATED_DATE,OBJECT_STATUS,OBJECT_VERSION_NUMBER,EDITABLE,CATEGORY_DESCRIPTION) values ('FCDB_SCHEMANAME','OriginationConfig','OBDX_SCHEMA','Y',null,'FCDB Registration Base URL','profinchuser',SYSDATE,'profinchuser',SYSDATE,'A',1,'N',null);

----- Please add OBDX schema password '123456'------------------
DELETE FROM DIGX_FW_CONFIG_ALL_B WHERE PROP_ID = 'FCDB_SCHEMAPASSWD' and CATEGORY_ID = 'OriginationConfig';
Insert into DIGX_FW_CONFIG_ALL_B (PROP_ID,CATEGORY_ID,PROP_VALUE,FACTORY_SHIPPED_FLAG,PROP_COMMENTS,SUMMARY_TEXT,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATED_DATE,OBJECT_STATUS,OBJECT_VERSION_NUMBER,EDITABLE,CATEGORY_DESCRIPTION) values ('FCDB_SCHEMAPASSWD','OriginationConfig','123456','Y',null,'FCDB Registration Base URL','profinchuser',SYSDATE,'profinchuser',SYSDATE,'A',1,'N',null);

-------------------------------------------------------
DELETE FROM DIGX_FW_CONFIG_ALL_O WHERE PROP_ID = 'com.profinch.digx.cz.extxface.origination.adapter.registration.ICZFcdbRegistration.validate';
Insert into DIGX_FW_CONFIG_ALL_O (PROP_ID,PREFERENCE_NAME,PROP_VALUE,DETERMINANT_VALUE,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATED_DATE) values ('com.profinch.digx.cz.extxface.origination.adapter.registration.ICZFcdbRegistration.validate','ExtxfaceAdapterPreference','com.profinch.digx.cz.extxface.origination.registration.impl.CZFCDBRegistrationAdapter','01','profinchuser',SYSDATE,'profinchuser',SYSDATE);
