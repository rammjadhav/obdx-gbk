RELEASE DOCUMENT - OBDX - GULF Bank
BUILD NO - 1915
BUILD FILE - GBK_DEV_1915.zip
BUILD DATE - 30-05-2021

OVERVIEW
Customization and Bug fixes for customizations to OBDX version 19.1 implemented at GULF Bank are included in this release.